import { Component, OnInit } from '@angular/core';
import { Person } from '../person';
import { PersonService } from '../person.service';


@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})

export class PersonComponent  implements OnInit {

  persons: Person[];

  constructor(private personService: PersonService) {}

  ngOnInit(): void {
     this.getPersons();
  }

  getPersons() {
    return this.personService.getPersons()
               .subscribe(
                 persons => {
                  console.log(persons);
                  this.persons = persons
                 }
                );
 }
}
