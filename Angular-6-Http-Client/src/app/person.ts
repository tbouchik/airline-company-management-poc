export class Person {
    id: number;
    firstname: string;
    lastname: string;
    address: string;
}
