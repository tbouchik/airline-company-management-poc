import { Component, OnInit } from '@angular/core';
import { Passenger } from '../passenger';
import { PassengerService } from '../passenger.service';


@Component({
  selector: 'app-passenger',
  templateUrl: './passenger.component.html',
  styleUrls: ['./passenger.component.css']
})

export class PassengerComponent  implements OnInit {

  passengers: Passenger[];

  constructor(private passengerService: PassengerService) {}

  ngOnInit(): void {
     this.getPassengers();
  }

  getPassengers() {
    return this.passengerService.getPassengers()
               .subscribe(
                 passengers => {
                  console.log(passengers);
                  this.passengers = passengers
                 }
                );
 }
}
