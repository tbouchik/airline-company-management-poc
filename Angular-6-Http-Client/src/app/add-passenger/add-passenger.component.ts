import { Component, OnInit } from '@angular/core';
import { Passenger } from '../passenger';
import { PassengerService } from '../passenger.service';

import { Location } from '@angular/common';

@Component({
  selector: 'app-add-passenger',
  templateUrl: './add-passenger.component.html',
  styleUrls: ['./add-passenger.component.css']
})

export class AddPassengerComponent{

  passenger = new Passenger();
  submitted = false;

  constructor(
    private passengerService: PassengerService,
    private location: Location
  ) { }

  newPassenger(): void {
    this.submitted = false;
    this.passenger = new Passenger();
  }

 addPassenger() {
   this.submitted = true;
   this.save();
 }

  goBack(): void {
    this.location.back();
  }

  private save(): void {
    console.log(this.passenger);
    this.passengerService.addPassenger(this.passenger)
        .subscribe();
  }
}
