import { Component, OnInit } from '@angular/core';
import { Passenger } from '../passenger';
import { PassengerService } from '../passenger.service';

import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-passenger-details',
  templateUrl: './passenger-details.component.html',
  styleUrls: ['./passenger-details.component.css']
})
export class PassengerDetailsComponent implements OnInit {

  passenger = new Passenger() ;
  submitted = false;
  message: string;

  constructor(
    private passengerService: PassengerService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.passengerService.getPassenger(id)
      .subscribe(passenger => {this.passenger = passenger});
  }

  update(): void {
    this.submitted = true;
    this.passengerService.updatePassenger(this.passenger)
        .subscribe(result => this.message = "Passenger Updated Successfully!");
  }

  delete(): void {
    this.submitted = true;
    this.passengerService.deletePassenger(this.passenger.id)
        .subscribe(result => this.message = "Passenger Deleted Successfully!");
  }

  goBack(): void {
    this.location.back();
  }
}