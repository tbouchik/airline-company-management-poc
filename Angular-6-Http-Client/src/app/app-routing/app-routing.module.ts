import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerComponent } from '../customer/customer.component';
import { AddCustomerComponent } from '../add-customer/add-customer.component';
import { CustomerDetailsComponent } from '../customer-details/customer-details.component';
import { PersonComponent } from '../person/person.component';
import { AddPersonComponent } from '../add-person/add-person.component';
import { PersonDetailsComponent } from '../person-details/person-details.component';
import { PassengerComponent } from '../passenger/passenger.component';
import { AddPassengerComponent } from '../add-passenger/add-passenger.component';
import { PassengerDetailsComponent } from '../passenger-details/passenger-details.component';

const routes: Routes = [
   { 
     path: 'customers', 
     component: CustomerComponent 
   },
   { 
     path: 'customer/add', 
     component: AddCustomerComponent 
   },
   { 
     path: 'customers/:id', 
     component: CustomerDetailsComponent 
   },
   { 
     path: 'persons', 
     component: PersonComponent 
   },
   { 
     path: 'person/add', 
     component: AddPersonComponent 
   },
   { 
     path: 'persons/:id', 
     component: PersonDetailsComponent 
   },
   { 
    path: 'passengers', 
    component: PassengerComponent 
  },
  { 
    path: 'passenger/add', 
    component: AddPassengerComponent 
  },
  { 
    path: 'passengers/:id', 
    component: PassengerDetailsComponent 
  },
   { 
     path: '', 
     redirectTo: 'persons', 
     pathMatch: 'full'
   }, 
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}