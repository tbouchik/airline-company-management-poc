import { Component, OnInit } from '@angular/core';
import { Person } from '../person';
import { PersonService } from '../person.service';

import { Location } from '@angular/common';

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.css']
})

export class AddPersonComponent{

  person = new Person();
  submitted = false;

  constructor(
    private personService: PersonService,
    private location: Location
  ) { }

  newPerson(): void {
    this.submitted = false;
    this.person = new Person();
  }

 addPerson() {
   this.submitted = true;
   this.save();
 }

  goBack(): void {
    this.location.back();
  }

  private save(): void {
    console.log(this.person);
    this.personService.addPerson(this.person)
        .subscribe();
  }
}
