import { Component, OnInit } from '@angular/core';
import { Person } from '../person';
import { PersonService } from '../person.service';

import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.css']
})
export class PersonDetailsComponent implements OnInit {

  person = new Person() ;
  submitted = false;
  message: string;

  constructor(
    private personService: PersonService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.personService.getPerson(id)
      .subscribe(person => {this.person = person});
  }

  update(): void {
    this.submitted = true;
    this.personService.updatePerson(this.person)
        .subscribe(result => this.message = "Person Updated Successfully!");
  }

  delete(): void {
    this.submitted = true;
    this.personService.deletePerson(this.person.id)
        .subscribe(result => this.message = "Person Deleted Successfully!");
  }

  goBack(): void {
    this.location.back();
  }
}