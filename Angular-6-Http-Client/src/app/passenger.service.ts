import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Passenger } from './passenger';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PassengerService {
  private passengersUrl = 'http://localhost:8080/api/passengers';  // URL to web api
  constructor( 
    private http: HttpClient
  ) { }

  getPassengers (): Observable<Passenger[]> {
    return this.http.get<Passenger[]>(this.passengersUrl)
  }

  getPassenger(id: number): Observable<Passenger> {
    const url = `${this.passengersUrl}/${id}`;
    console.log('executing http get : ', url);
    return this.http.get<Passenger>(url);
  }

  addPassenger (passenger: Passenger): Observable<Passenger> {
    return this.http.post<Passenger>(this.passengersUrl, passenger, httpOptions);
  }

  deletePassenger (passenger: Passenger | number): Observable<Passenger> {
    const id = typeof passenger === 'number' ? passenger : passenger.id;
    const url = `${this.passengersUrl}/${id}`;

    return this.http.delete<Passenger>(url, httpOptions);
  }

  updatePassenger (passenger: Passenger): Observable<any> {
    return this.http.put(this.passengersUrl, passenger, httpOptions);
  }
}