import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Person } from './person';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private personsUrl = 'http://localhost:8080/api/persons';  // URL to web api
  constructor( 
    private http: HttpClient
  ) { }

  getPersons (): Observable<Person[]> {
    return this.http.get<Person[]>(this.personsUrl)
  }

  getPerson(id: number): Observable<Person> {
    const url = `${this.personsUrl}/${id}`;
    console.log('executing http get : ', url);
    return this.http.get<Person>(url);
  }

  addPerson (person: Person): Observable<Person> {
    return this.http.post<Person>(this.personsUrl, person, httpOptions);
  }

  deletePerson (person: Person | number): Observable<Person> {
    const id = typeof person === 'number' ? person : person.id;
    const url = `${this.personsUrl}/${id}`;

    return this.http.delete<Person>(url, httpOptions);
  }

  updatePerson (person: Person): Observable<any> {
    return this.http.put(this.personsUrl, person, httpOptions);
  }
}