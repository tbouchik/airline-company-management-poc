var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json())
 
const cors = require('cors')
const corsOptions = {
  origin: 'http://localhost:4200',
  optionsSuccessStatus: 200
}
 
app.use(cors(corsOptions))
 
const db = require('./app/config/db.config.js');
  
// force: true will drop the table if it already exists
db.sequelize.sync({force: true}).then(() => {
  console.log('Drop and Resync with { force: true }');
  initial();
});
 
require('./app/route/person.route.js')(app);
require('./app/route/customer.route.js')(app);
require('./app/route/passenger.route.js')(app);
// Create a Server
var server = app.listen(8080, function () {
 
  let host = server.address().address
  let port = server.address().port
 
  console.log("App listening at http://%s:%s", host, port);
})
 
function initial(){
 
  let passengers = [
    {
      firstname:"Cronvelious",
      lastname:"Daniel",
      address:"4197 Broaddus Avenue"
    },
    {
      firstname:"King Flippy ",
      lastname:"Nips",
      address:"3329 Pluto Is A Planet"
    },
    {
      firstname:"Squanchy",
      lastname:"Squanch",
      address:"3912 Squanch Squanch Road"
    }
  ];
  let persons = [
    {
      firstname:"Cronvelious",
      lastname:"Daniel",
      address:"4197 Broaddus Avenue"
    },
    {
      firstname:"King Flippy ",
      lastname:"Nips",
      address:"3329 Pluto Is A Planet"
    },
    {
      firstname:"Squanchy",
      lastname:"Squanch",
      address:"3912 Squanch Squanch Road"
    },
    {
      firstname:"Unity",
      lastname:"N/A",
      address:"Planet Unity"
    },
    {
      firstname:"Summer",
      lastname:"Smith",
      address:"4622 Old House Drive"
    },
    {
      firstname:"Reverse",
      lastname:"Giraffe",
      address:"3900 Jehovah Drive"
    }
  ]


  // Init data -> save to MySQL
  const Person = db.persons;
  for (let i = 0; i < persons.length; i++) { 
    Person.create(persons[i]);  
  }

  const Passenger = db.passengers;
  for (let i = 0; i < passengers.length; i++) { 
    Passenger.create(passengers[i]);  
  }
}