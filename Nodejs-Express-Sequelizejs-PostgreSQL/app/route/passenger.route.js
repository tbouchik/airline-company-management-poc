module.exports = function(app) {
    const passengers = require('../controller/passenger.controller.js');
 
    // Create a new Passenger
    app.post('/api/passengers', passengers.create);
 
    // Retrieve all Passenger
    app.get('/api/passengers', passengers.findAll);
 
    // Retrieve a single Passenger by Id
    app.get('/api/passengers/:id', passengers.findById);
 
    // Update a Passenger with Id
    app.put('/api/passengers', passengers.update);
 
    // Delete a Passenger with Id
    app.delete('/api/passengers/:id', passengers.delete);
}