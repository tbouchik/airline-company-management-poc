module.exports = function(app) {
    const persons = require('../controller/person.controller.js');
 
    // Create a new Person
    app.post('/api/persons', persons.create);
 
    // Retrieve all Person
    app.get('/api/persons', persons.findAll);
 
    // Retrieve a single Person by Id
    app.get('/api/persons/:id', persons.findById);
 
    // Update a Person with Id
    app.put('/api/persons', persons.update);
 
    // Delete a Person with Id
    app.delete('/api/persons/:id', persons.delete);
}