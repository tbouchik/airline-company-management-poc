const db = require('../config/db.config.js');
const Person = db.persons;

// Post a Person
exports.create = (req, res) => {	
	// Save to PostgreSQL database
	Person.create({
				"firstname": req.body.firstname, 
				"lastname": req.body.lastname, 
				"address": req.body.address
			}).then(person => {		
			// Send created person to client
			res.json(person);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};
 
// FETCH All Persons
exports.findAll = (req, res) => {
	Person.findAll().then(persons => {
			// Send All Persons to Client
			res.json(persons.sort(function(c1, c2){return c1.id - c2.id}));
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};

// Find a Person by Id
exports.findById = (req, res) => {	
	Person.findById(req.params.id).then(person => {
			res.json(person);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};
 
// Update a Person
exports.update = (req, res) => {
	const id = req.body.id;
	Person.update( req.body, 
			{ where: {id: id} }).then(() => {
				res.status(200).json( { mgs: "Updated Successfully -> Person Id = " + id } );
			}).catch(err => {
				console.log(err);
				res.status(500).json({msg: "error", details: err});
			});
};

// Delete a Person by Id
exports.delete = (req, res) => {
	const id = req.params.id;
	Person.destroy({
			where: { id: id }
		}).then(() => {
			res.status(200).json( { msg: 'Deleted Successfully -> Person Id = ' + id } );
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};