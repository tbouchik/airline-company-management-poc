const db = require('../config/db.config.js');
const Passenger = db.passengers;

// Post a Passenger
exports.create = (req, res) => {	
	// Save to PostgreSQL database
	Passenger.create({
				"firstname": req.body.firstname, 
				"lastname": req.body.lastname, 
				"address": req.body.address
			}).then(passenger => {		
			// Send created passenger to client
			res.json(passenger);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};
 
// FETCH All Passengers
exports.findAll = (req, res) => {
	Passenger.findAll().then(passengers => {
			// Send All Passengers to Client
			res.json(passengers.sort(function(c1, c2){return c1.id - c2.id}));
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};

// Find a Passenger by Id
exports.findById = (req, res) => {	
	Passenger.findById(req.params.id).then(passenger => {
			res.json(passenger);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};
 
// Update a Passenger
exports.update = (req, res) => {
	const id = req.body.id;
	Passenger.update( req.body, 
			{ where: {id: id} }).then(() => {
				res.status(200).json( { mgs: "Updated Successfully -> Passenger Id = " + id } );
			}).catch(err => {
				console.log(err);
				res.status(500).json({msg: "error", details: err});
			});
};

// Delete a Passenger by Id
exports.delete = (req, res) => {
	const id = req.params.id;
	Passenger.destroy({
			where: { id: id }
		}).then(() => {
			res.status(200).json( { msg: 'Deleted Successfully -> Passenger Id = ' + id } );
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};