/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('onboardingmember', {
    firstname: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'onairworker',
        key: 'firstname'
      }
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    function: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'onboardingmember'
  });
};
