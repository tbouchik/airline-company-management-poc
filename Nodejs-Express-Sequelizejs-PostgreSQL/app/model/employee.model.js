/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('employee', {
    firstname: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'person',
        key: 'firstname'
      },
      unique: true
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    secuid: {
      type: DataTypes.INTEGER,
      allowNull: true,
      unique: true
    },
    salary: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    tableName: 'employee'
  });
};
