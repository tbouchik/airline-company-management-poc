/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('aircraft', {
    idnumber: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    aircrafttype: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'aircraft'
  });
};
