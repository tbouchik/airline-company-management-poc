/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('reservation', {
    firstname: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'passenger',
        key: 'firstname'
      }
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    ticketnbr: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'flightticket',
        key: 'ticketnbr'
      },
      unique: true
    }
  }, {
    tableName: 'reservation'
  });
};
