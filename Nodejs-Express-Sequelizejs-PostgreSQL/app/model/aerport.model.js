/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('aerport', {
    aerportcode: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    aerportname: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'aerport'
  });
};
