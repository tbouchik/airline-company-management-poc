/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('onairworker', {
    firstname: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'employee',
        key: 'firstname'
      },
      unique: true
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    flighthours: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    tableName: 'onairworker'
  });
};
