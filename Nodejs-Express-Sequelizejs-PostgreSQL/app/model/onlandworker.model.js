/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('onlandworker', {
    firstname: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'employee',
        key: 'firstname'
      }
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'onlandworker'
  });
};
