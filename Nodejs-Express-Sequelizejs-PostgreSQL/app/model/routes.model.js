/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('routes', {
    routenbr: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    fromarpt: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'aerport',
        key: 'aerportcode'
      }
    },
    toarpt: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'aerport',
        key: 'aerportcode'
      }
    }
  }, {
    tableName: 'routes'
  });
};
