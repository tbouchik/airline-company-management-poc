/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pilot', {
    firstname: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'onairworker',
        key: 'firstname'
      },
      unique: true
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    licencenbr: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'pilot'
  });
};
