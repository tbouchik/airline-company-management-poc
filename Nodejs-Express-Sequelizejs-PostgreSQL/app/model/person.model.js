/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('person', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    firstname: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
      unique:true
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
      unique:true

    },
    address: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'person',
    timestamps: false,
  });
};
