/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('flight', {
    flightnbr: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    flightperiod: {
      type: DataTypes.DATE,
      allowNull: true
    },
    assignedroute: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'routes',
        key: 'routenbr'
      }
    },
    aircraft: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'aircraft',
        key: 'idnumber'
      }
    }
  }, {
    tableName: 'flight'
  });
};
