/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('departure', {
    departurenbr: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    departuredate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    freeseats: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    takenseats: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    flight: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'flight',
        key: 'flightnbr'
      }
    }
  }, {
    tableName: 'departure'
  });
};
