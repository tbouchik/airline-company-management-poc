/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('passenger', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
      unique:true
    },
    firstname: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'person',
        key: 'firstname'
      },
      unique: true
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    identificationnbr: {
      type: DataTypes.INTEGER,
      allowNull: true,
      unique: true
    }
  }, {
    tableName: 'passenger',
    timestamp:false
  });
};
