/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('flightpilots', {
    firstname: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'pilot',
        key: 'firstname'
      }
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    departurenbr: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'departure',
        key: 'departurenbr'
      }
    }
  }, {
    tableName: 'flightpilots'
  });
};
