/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('flightticket', {
    ticketnbr: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    emissiondate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    price: {
      type: DataTypes.REAL,
      allowNull: true
    },
    departure: {
      type: DataTypes.STRING,
      allowNull: true,
      references: {
        model: 'departure',
        key: 'departurenbr'
      }
    }
  }, {
    tableName: 'flightticket'
  });
};
